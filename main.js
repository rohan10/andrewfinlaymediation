'use strict';

const monthMap = {
  January: '01', February: '02', March: '03',
  April: '04', May: '05', June: '06',
  July: '07', August: '08', September: '09',
  October: '10', November: '11', December: '12'
};

const apptMap = { '(a)': 'Afternoon', '(m)': 'Morning' }

const replaceYear = val => val.replace('2017', '').replace(',','');
const removeAnd = val => val !== 'and';
const getDate = val => String(val).length === 2 ? String(val) : '0' + val

function getObj(val) {
  const idx = val.indexOf('(');
  return {
    date: getDate(val.slice(0, idx)),
    title: apptMap[val.slice(idx)]
  };
}

function getFinalVals(arr) {
  let year = arr[1].slice(0, 4);
  let month = monthMap[arr[0]];
  return arr
    .slice(1)
    .map((val, i) => {
      if (i === 0) return val.slice(4).replace(',','')
      return val.replace(',','')
    })
    .filter(val => val !== 'and')
    .reduce((acc, val) => {
      if (String(parseInt(val)).length !== val.length) {
        let obj = getObj(val);
        acc.push(obj);
      } else {
        acc.push({
          date: getDate(val),
          title: 'Morning'
        });

        acc.push({
          date: getDate(val),
          title: 'Afternoon'
        });
      }
      return acc;
    }, [])
    .map(val => Object.assign(val, {
      date: `${year}-${month}-${val.date}`
    }))
}

$(document).ready(function() {

  $.get('http://www.mediatordates.com/printtext.php?m=875')
    .then(function(data) {
      let mainArr = [];
      $(data.results[0]).find('.ad').each(function(i, val) {
        const textVals = $(val).text().split(' ');
        const dateArr = getFinalVals(textVals);
        mainArr = mainArr.concat(dateArr);
      });
      return mainArr
    })
    .then(function(dates) {
      $('#calendar').fullCalendar({
        weekends: false,
        events: dates
      });
    })
});
